vimv
====


## About

This is a fork of [`vimv`](https://github.com/thameera/vimv) script with the following changes:

  - allows deleting files by changing their filename to an empty line (deleted line will result in an error);
  - if `$TRASH` env var is defined, then instead of deleting files, moves them to `$TRASH`.

## Credits

All the credits, of course, go to the [original authors](https://github.com/thameera/vimv/graphs/contributors).
